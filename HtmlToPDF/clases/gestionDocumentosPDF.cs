﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using System.Text.RegularExpressions;
using System.Collections;
using System.Xml.Serialization;
using Windows.UI.Input;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;

using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.tool.xml.exceptions;
using iTextSharp.tool.xml.css.apply;
using iTextSharp.tool.xml.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System.IO;
using Windows.UI.Xaml.Media.Imaging;




namespace HtmlToPDF.clases
{
    class gestionDocumentosPDF
    {
        public StorageFolder directorio_origen_html;
        public StorageFolder directorio_origen_css;
        public StorageFolder directorio_origen_imagenes;
        public StorageFolder directorio_destino;
        public string nombreDocumentoPDF;
        public string nombreDocHtml;
        public List<string> nombreDocCss;

        #region Constructores
        public gestionDocumentosPDF()
        {
            directorio_origen_html = ApplicationData.Current.LocalFolder;
            directorio_origen_css = ApplicationData.Current.LocalFolder;
            directorio_origen_imagenes = ApplicationData.Current.LocalFolder;
            directorio_destino = ApplicationData.Current.LocalFolder;
        }
        public gestionDocumentosPDF(StorageFolder dir_destino)
        {
            directorio_origen_html = ApplicationData.Current.LocalFolder;
            directorio_destino = dir_destino;
        }
        public gestionDocumentosPDF(StorageFolder dir_html, StorageFolder dir_destino)
        {
            directorio_origen_html = dir_html;
            directorio_origen_css = ApplicationData.Current.LocalFolder;
            directorio_origen_imagenes = ApplicationData.Current.LocalFolder;
            directorio_destino = dir_destino;
        }
        public gestionDocumentosPDF(StorageFolder dir_html, StorageFolder dir_css, StorageFolder dir_destino)
        {
            directorio_origen_html = dir_html;
            directorio_origen_css = dir_css;
            directorio_origen_imagenes = ApplicationData.Current.LocalFolder;
            directorio_destino = dir_destino;
        }
        public gestionDocumentosPDF(StorageFolder dir_html, StorageFolder dir_css, StorageFolder dir_imagenes, StorageFolder dir_destino)
        {
            directorio_origen_html = dir_html;
            directorio_origen_css = dir_css;
            directorio_origen_imagenes = dir_imagenes;
            directorio_destino = dir_destino;
        }
        #endregion

        #region Parámetros
        public StorageFolder Directorio_Origen_HTML
        {
            get
            {
                return this.directorio_origen_html;
            }
            set
            {
                this.directorio_origen_html = value;
            }
        }
        public StorageFolder Directorio_Origen_CSS
        {
            get
            {
                return this.directorio_origen_css;
            }
            set
            {
                this.directorio_origen_css = value;
            }
        }
        public StorageFolder Directorio_Origen_IMAGENES
        {
            get
            {
                return this.directorio_origen_imagenes;
            }
            set
            {
                this.directorio_origen_imagenes = value;
            }
        }
        public StorageFolder Directorio_Destino
        {
            get
            {
                return this.directorio_destino;
            }
            set
            {
                this.directorio_destino = value;
            }
        }
        public string NombreDocumentoPDF
        {
            get
            {
                return this.nombreDocumentoPDF;
            }
            set
            {
                this.nombreDocumentoPDF = value;
            }
        }
        public string NombreDocHtml
        {
            get
            {
                return this.nombreDocHtml;
            }
            set
            {
                this.nombreDocHtml = value;
            }
        }
        public List<string> NombreDocCss
        {
            get
            {
                return this.nombreDocCss;
            }
            set
            {
                this.nombreDocCss = value;
            }
        }
        #endregion

        #region Métodos
        //public async Task<bool> transformar_html_B()
        //{
        //    bool exito = true;

        //    try
        //    {
        //        //obtener documentos como streams de datos
        //        StorageFile docHtml = await directorio_origen_html.GetFileAsync(nombreDocHtml);
        //        StorageFile docCss;
        //        IRandomAccessStream streamHtml = await docHtml.OpenAsync(FileAccessMode.ReadWrite);
        //        //IRandomAccessStream streamCss = await docCss.OpenAsync(FileAccessMode.ReadWrite);

        //        string cadenaCSS = String.Empty;

        //        foreach (string s in nombreDocCss)
        //        {
        //            docCss = await directorio_origen_css.GetFileAsync(s);
        //            cadenaCSS = await FileIO.ReadTextAsync(docCss, Windows.Storage.Streams.UnicodeEncoding.Utf8);
        //            cadenaCSS = cadenaCSS.Replace("\n", " ").Replace("\t", " ").Replace("\r", " ");
        //        }
        //        MemoryStream streamCss = new MemoryStream(Encoding.Unicode.GetBytes(cadenaCSS));

        //        //crear el documento pdf de salida.
        //        Document doc = new Document();
        //        StorageFile pdfOutputFile = await directorio_destino.CreateFileAsync(String.Concat(DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss"),"_doc_", nombreDocumentoPDF, ".pdf"), CreationCollisionOption.ReplaceExisting);
        //        IRandomAccessStream outputPdfStream = await pdfOutputFile.OpenAsync(FileAccessMode.ReadWrite);
        //        PdfWriter writer = PdfWriter.GetInstance(doc, outputPdfStream.AsStream());

        //        //abrir el documento e insertar en él los datos.
        //        doc.Open();

        //        //transformación
        //        iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, streamHtml.AsStream(), streamCss);
        //        //iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, streamHtml.AsStream(), Encoding.UTF8);

        //        //cerrar los streams de datos
        //        doc.Close();
        //        writer.Close();
        //        streamCss.Dispose();
        //        streamHtml.Dispose();
        //        outputPdfStream.Dispose();
        //    }
        //    catch (Exception ex)
        //    {
        //        exito = false;
        //    }

        //    return exito;
        //}

        public async Task<bool> transformar_html()
        {
            bool exito = true;
            htmlToPDF insertadorImagenes = new htmlToPDF();


            try
            {
                //obtener documentos como streams de datos
                StorageFile docHtml = await directorio_origen_html.GetFileAsync(nombreDocHtml);

                //insertar imágenes en el html
                insertadorImagenes.directorio_origen = directorio_origen_html;
                exito = await insertadorImagenes.insertarImagenesEnHTML(directorio_origen_imagenes, docHtml);


                //crear el documento pdf de salida.              
                StorageFile pdfOutputFile = await directorio_destino.CreateFileAsync(String.Concat(DateTimeOffset.Now.ToString("MM_dd_yy_hh_mm_ss"), "_doc_", nombreDocumentoPDF, ".pdf"), CreationCollisionOption.ReplaceExisting);
                Stream pdfStream = await pdfOutputFile.OpenStreamForWriteAsync();

                using (Document doc = new Document())
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                    {
                        doc.Open();

                        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                        htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                        ImSignImageProvider imageProvider = new ImSignImageProvider();
                        htmlContext.SetImageProvider(imageProvider);

                        ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(false);

                        //añadimos la referencia al fichero de estilos 
                        foreach (string s in nombreDocCss)
                        {
                            StorageFile docCss = await directorio_origen_css.GetFileAsync(s);
                            IRandomAccessStream streamCss = await docCss.OpenAsync(FileAccessMode.ReadWrite);
                            cssResolver.AddCss(XMLWorkerHelper.GetCSS(streamCss.AsStream()));
                            streamCss.Dispose();
                        }                        

                        IPipeline pipeline =
                        new CssResolverPipeline(cssResolver,
                                new HtmlPipeline(htmlContext,
                                    new PdfWriterPipeline(doc, writer)));

                        XMLWorker worker = new XMLWorker(pipeline, true);
                        XMLParser p = new XMLParser(worker);

                        try
                        {
                            using (var stream = await docHtml.OpenAsync(FileAccessMode.Read))
                            {
                                var reader = new DataReader(stream.GetInputStreamAt(0));
                                var bytes = new byte[stream.Size];
                                await reader.LoadAsync((uint)stream.Size);
                                reader.ReadBytes(bytes);

                                p.Parse(stream.AsStream());
                            }
                        }
                        catch (Exception e)
                        {
                            string error = e.ToString();
                        }

                        //cerrar los streams de datos
                        doc.Close();                        
                        pdfStream.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                exito = false;
            }

            return exito;
        }
        
        #endregion
    }
}
