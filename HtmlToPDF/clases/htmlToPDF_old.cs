﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//For converting HTML TO PDF- START
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool;
using iTextSharp.tool.xml;
using System.IO;
using System.util;
using System.Text.RegularExpressions;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
//For converting HTML TO PDF- END

using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.html.table;
using iTextSharp.tool.xml.pipeline;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.css.parser;
using iTextSharp.tool.xml.css.apply;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.parser.io;
using iTextSharp.tool.xml.parser.state;


namespace HtmlToPDF.clases
{
    public class htmlToPDF
    {
        public StorageFolder directorio_origen;
        public StorageFolder directorio_destino;
        public StorageFile pdfOutputFile;
        public string cadenaHTML;
        public string cadenaCSS;
        public static String base64;
        public static Dictionary<string, string> dicBase64;

        public htmlToPDF()
        {
            
        }

        public async Task<Boolean> convertImageToBase64(StorageFolder location, string fileName)
        {
            bool exito = true;

            try
            {
                StorageFile imageFile = await location.GetFileAsync(fileName);

                using (var stream = await imageFile.OpenAsync(FileAccessMode.Read))
                {
                    var reader = new DataReader(stream.GetInputStreamAt(0));
                    var bytes = new byte[stream.Size];
                    await reader.LoadAsync((uint)stream.Size);

                    reader.ReadBytes(bytes);
                    base64 = Convert.ToBase64String(bytes);
                }
            }
            catch(Exception)
            {
                exito = false;
            }

            return exito;
        }

        public async Task<Boolean> insertarImagenesEnHTML (StorageFolder location, string nomHTML, string nomImg)
        {
            bool exito = true;

            try
            {
                //convertimos la imagen a base64
                Boolean exitoImagen = await convertImageToBase64(location, nomImg);
                StorageFile sfHTML = await location.GetFileAsync(nomHTML);
                string strHTML = await FileIO.ReadTextAsync(sfHTML, Windows.Storage.Streams.UnicodeEncoding.Utf8);
                strHTML = strHTML.Replace("\n", " ").Replace("\t", " ").Replace("\r", " ");

                //reemplazamos la etiqueta img original por la nueva con la imagen en base64
                strHTML = strHTML.Replace("<img src=\"" + nomImg + "\" />", "<img src=\"data:image/png;base64," + base64 + "\" />");

                await FileIO.WriteTextAsync(sfHTML, strHTML);

                //¡IMPORTANTE!
                //Falta la parte de sustituir el texto html del documento por el nuevo contenido en strHTML.

                #region CODIGO anterior - María
                ////prueba -- construimos el html directamente
                // StringBuilder sb = new StringBuilder();

                // sb.Append(@"<!DOCTYPE html>" + "\r\n");
                // sb.Append(@"<html xmlns=""http://www.w3.org/1999/xhtml"">" + "\r\n");
                // sb.Append(@"<head>" + "\r\n");
                // sb.Append(@"<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""/>" + "\r\n");
                // sb.Append(@"<title></title>" + "\r\n");
                // sb.Append(@"</head>" + "\r\n");
                // sb.Append(@"<body>" + "\r\n");
                // sb.Append(@"<header>" + "\r\n");
                // sb.Append(@"<h1 >Esta es una página de prueba para Mémora</h1>" + "\r\n");
                // sb.Append(@"</header>" + "\r\n");
                // sb.Append(@"<section>" + "\r\n");
                // sb.Append(@"<p >Parrafo de prueba con estilos</p>" + "\r\n");
                // sb.Append(@"<p >Parrafo de prueba con estilos - prueba 2</p>" + "\r\n");
                // sb.Append(@"</section>" + "\r\n");
                // sb.Append(@"<footer>" + "\r\n");
                // sb.Append("<img src=\"data:image/png;base64,");
                // sb.Append(base64);
                // sb.Append("\" />");
                //byte[] bytesPDF = obtenerDocumentoComoArrayDeBytes(strHTML, strCSS);  
                #endregion
            }
            catch(Exception ex)
            {
                exito = false;
            }

            return exito;
        }
        public async Task<Boolean> insertarImagenesEnHTML(StorageFolder location, string nomHTML, List<string> listaNomImagenes)
        {
            bool exito = true;

            try
            {
                //convertimos las imagenes a base64
                foreach (string nomImg in listaNomImagenes)
                {
                    Boolean exitoImagen = await convertImageToBase64(location, nomImg);
                    
                    if(exitoImagen)
                    {
                        dicBase64.Add(nomImg, base64);
                    }
                }
                StorageFile sfHTML = await location.GetFileAsync(nomHTML);
                IRandomAccessStream sHTML = await sfHTML.OpenAsync(FileAccessMode.ReadWrite);
                string strHTML = String.Empty;

                using (StreamReader sr = new StreamReader(sHTML.AsStream()))
                {
                    strHTML = sr.ReadToEnd();
                }

                //reemplazamos las etiquetas img originaesl por las nuevas con las imagenes en base64
                foreach (KeyValuePair<string, string> kv in dicBase64)
                {
                    strHTML = strHTML.Replace("<img src=\"" + kv.Key + "\" />", "<img src=\"data:image/png;base64," + kv.Value + "\" />");
                }

                //¡IMPORTANTE!
                //Falta la parte de sustituir el texto html del documento por el nuevo contenido en strHTML.

                sHTML.Dispose();

                #region CODIGO anterior - María
                ////prueba -- construimos el html directamente
                // StringBuilder sb = new StringBuilder();

                // sb.Append(@"<!DOCTYPE html>" + "\r\n");
                // sb.Append(@"<html xmlns=""http://www.w3.org/1999/xhtml"">" + "\r\n");
                // sb.Append(@"<head>" + "\r\n");
                // sb.Append(@"<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""/>" + "\r\n");
                // sb.Append(@"<title></title>" + "\r\n");
                // sb.Append(@"</head>" + "\r\n");
                // sb.Append(@"<body>" + "\r\n");
                // sb.Append(@"<header>" + "\r\n");
                // sb.Append(@"<h1 >Esta es una página de prueba para Mémora</h1>" + "\r\n");
                // sb.Append(@"</header>" + "\r\n");
                // sb.Append(@"<section>" + "\r\n");
                // sb.Append(@"<p >Parrafo de prueba con estilos</p>" + "\r\n");
                // sb.Append(@"<p >Parrafo de prueba con estilos - prueba 2</p>" + "\r\n");
                // sb.Append(@"</section>" + "\r\n");
                // sb.Append(@"<footer>" + "\r\n");
                // sb.Append("<img src=\"data:image/png;base64,");
                // sb.Append(base64);
                // sb.Append("\" />");
                //byte[] bytesPDF = obtenerDocumentoComoArrayDeBytes(strHTML, strCSS);  
                #endregion
            }
            catch (Exception)
            {
                exito = false;
            }

            return exito;
        }

        public Byte[] obtenerDocumentoComoArrayDeBytes(string html, string css)
        {
            byte[] resultado; //contendrá el documento PDF final.
            byte[] htmlCad = System.Text.Encoding.UTF8.GetBytes(html);
            byte[] cssCad = System.Text.Encoding.UTF8.GetBytes(css);

            using (MemoryStream ms = new MemoryStream())
            {
                using (Document doc = new Document())
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, ms))
                    {
                        doc.Open();

                        using (MemoryStream msCss = new MemoryStream(cssCad))
                        {
                            using (MemoryStream msHtml = new MemoryStream(htmlCad))
                            {                                
                                iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msHtml, msCss);
                            }
                        }

                        doc.Close();
                    }
                }

                resultado = ms.ToArray();
            }

            return resultado;
        }

        public String getImageRootPath()
        {
            return "";
        }


        public async Task<Byte[]> obtenerDocumentoComoArrayDeBytes(string cadenaHTML)
        {
            byte[] resultado = null; //contendrá el documento PDF final.
            byte[] htmlCad = System.Text.Encoding.UTF8.GetBytes(cadenaHTML);
            //byte[] cssCad = System.Text.Encoding.UTF8.GetBytes(cadenaCSS);

            //StorageFolder carpeta = await StorageFolder.GetFolderFromPathAsync(rutaCarpeta);
            //StorageFile docHtml = await carpeta.GetFileAsync(nombreDocHTML);

            //using (MemoryStream ms = new MemoryStream())
            //{

            StorageFile pdf = await ApplicationData.Current.LocalFolder.CreateFileAsync("PDFmaria.pdf", CreationCollisionOption.ReplaceExisting);
            Stream pdfStream = await pdf.OpenStreamForWriteAsync();

            using (Document doc = new Document())
            {


                using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                {
                    doc.Open();

                    HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                    htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                    ImSignImageProvider imageProvider = new ImSignImageProvider();
                    htmlContext.SetImageProvider(imageProvider);

                    ICSSResolver cssResolver =   XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                    IPipeline pipeline =
                    new CssResolverPipeline(cssResolver,
                            new HtmlPipeline(htmlContext,
                                new PdfWriterPipeline(doc, writer)));

                    XMLWorker worker = new XMLWorker(pipeline, true);
                    XMLParser p = new XMLParser(worker);

                    // Get the input stream for the SessionState file
                    StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("pagina_prueba.html");

                    try
                    {
                        using (var stream = await file.OpenAsync(FileAccessMode.Read))
                        {
                            var reader = new DataReader(stream.GetInputStreamAt(0));
                            var bytes = new byte[stream.Size];
                            await reader.LoadAsync((uint)stream.Size);
                            reader.ReadBytes(bytes);
                                
                            p.Parse(stream.AsStream());
                        }
                    }
                    catch (Exception e)
                    {
                        string error = e.ToString();
                    }
                        
                    doc.Close();                              
                }
            }

            //    resultado = ms.ToArray();
            //}

            return resultado;
        }

        public async Task<Byte[]> obtenerDocumentoComoArrayDeBytes2(string cadenaHTML)
        {
            byte[] resultado = null; //contendrá el documento PDF final.
            byte[] htmlCad = System.Text.Encoding.UTF8.GetBytes(cadenaHTML);

            StorageFile pdf = await ApplicationData.Current.LocalFolder.CreateFileAsync("PDFmaria.pdf", CreationCollisionOption.ReplaceExisting);
            Stream pdfStream = await pdf.OpenStreamForWriteAsync();

            using (Document doc = new Document())
            {
                using (PdfWriter writer = PdfWriter.GetInstance(doc, pdfStream))
                {
                    doc.Open();

                    HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
                    htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());

                    ImSignImageProvider imageProvider = new ImSignImageProvider();
                    htmlContext.SetImageProvider(imageProvider);                   

                    ICSSResolver cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);

                    IPipeline pipeline =
                    new CssResolverPipeline(cssResolver,
                            new HtmlPipeline(htmlContext,
                                new PdfWriterPipeline(doc, writer)));

                    XMLWorker worker = new XMLWorker(pipeline, true);
                    XMLParser p = new XMLParser(worker);

                    StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("pruebaHTMLMaria.html");

                    try
                    {
                        using (var stream = await file.OpenAsync(FileAccessMode.Read))
                        {
                            var reader = new DataReader(stream.GetInputStreamAt(0));
                            var bytes = new byte[stream.Size];
                            await reader.LoadAsync((uint)stream.Size);
                            reader.ReadBytes(bytes);

                            p.Parse(stream.AsStream());
                        }
                    }
                    catch (Exception e)
                    {
                        string error = e.ToString();
                    }

                    doc.Close();
                }
            }

            return resultado;
        }
    } 
}
