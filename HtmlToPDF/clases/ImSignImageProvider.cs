﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

using iTextSharp.tool.xml.pipeline.html;
using Windows.Security.Cryptography;

namespace HtmlToPDF.clases
{
   

    public class ImSignImageProvider : AbstractImageProvider
    {
        public override string GetImageRootPath()
        {
           // return "C:/Users/jamartinm/AppData/Local/Packages/895c6838-5031-4baf-b381-89e1e59e245c_0c9yd2j9am570/LocalState/";
            //return ApplicationData.Current.LocalFolder.Path;

            return null;
        }

        public override iTextSharp.text.Image Retrieve(string src)
        {
            //return base.Retrieve(src);
            int pos = src.IndexOf("base64,");

            try
            {
                if (src.StartsWith("data") && pos > 0)
                {
                    //byte[] img = Convert.ToByte(src.Substring(pos + 7));

                    var dataBuffer = CryptographicBuffer.DecodeFromBase64String(src.Substring(pos + 7));
                    byte[] img;
                    CryptographicBuffer.CopyToByteArray(dataBuffer, out img);
                    return iTextSharp.text.Image.GetInstance(img);
                }
                else
                {
                    return iTextSharp.text.Image.GetInstance(src);
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
