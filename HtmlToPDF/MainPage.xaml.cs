﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage.Pickers;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using HtmlToPDF.clases;
using Windows.UI.Popups;

// La plantilla de elemento Página en blanco está documentada en http://go.microsoft.com/fwlink/?LinkId=234238

namespace HtmlToPDF
{
    /// <summary>
    /// Página vacía que se puede usar de forma independiente o a la que se puede navegar dentro de un objeto Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private gestionDocumentosPDF gestionPDF;
        private StorageFolder carpetaActual;
        private string rutaHTML;

        private string nombreHTML;

        public MainPage()
        {
            this.InitializeComponent();
        }


        private async void Browse_Click(object sender, RoutedEventArgs e)
        {
            // create FileOpen picker with filter .pfx
            FileOpenPicker filePicker = new FileOpenPicker();
            filePicker.FileTypeFilter.Add(".html");
            filePicker.CommitButtonText = "Seleccionar";

            

            try
            {
                StorageFile file = await filePicker.PickSingleFileAsync();
                if (file != null)
                {
                    // file was picked and is available for read
                    // try to read the file content
                    IBuffer buffer = await FileIO.ReadBufferAsync(file);
                    using (DataReader dataReader = DataReader.FromBuffer(buffer))
                    {
                        byte[] bytes = new byte[buffer.Length];
                        dataReader.ReadBytes(bytes);
                       
                    }

                    this.HTMLtb.Text = file.Path;

                    nombreHTML = file.Name;

                    rutaHTML = file.Path.Substring(0, ((file.Path.Length) - (file.Name.Length + 1)));

                    carpetaActual = await StorageFolder.GetFolderFromPathAsync(rutaHTML);

                   
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void GenerarPDF_Click(object sender, RoutedEventArgs e)
        {
            generar_pdf_desde_html();
        }

        private async Task<string> generar_pdf_desde_html()
        {
            string resultado;
            List<string> listaCss = new List<string>();


            


            try
            {

              
                //StorageFolder sfHTML = await carpetaActual.GetFolderAsync("template_html_to_pdf");
                StorageFolder sfCSS = await carpetaActual.GetFolderAsync("css");
                StorageFolder sfIMG = await carpetaActual.GetFolderAsync("images");

                htmlToPDF htmlTOpdf = new htmlToPDF();
                gestionPDF = new gestionDocumentosPDF(carpetaActual, sfCSS, sfIMG, carpetaActual);
                gestionPDF.nombreDocumentoPDF = "prueba_itextsharp"; //nombre del documento pdf de salida
                gestionPDF.nombreDocHtml = nombreHTML; //nombre del html de entrada

                var listadoCss = await sfCSS.GetItemsAsync();
                             


                foreach(StorageFile cssFile in listadoCss)
                {
                    listaCss.Add(cssFile.Name);
                }


                gestionPDF.nombreDocCss = listaCss; //nombres de los css asociados al html de entrada

                //bool exito = await htmlTOpdf.insertarImagenesEnHTML(carpetaActual, gestionPDF.nombreDocHtml, "btnfits_04.png");
                bool exito_generar = await gestionPDF.transformar_html();

                resultado = String.Concat("Se ha creado con éxito el documento PDF ",
                                            gestionPDF.nombreDocumentoPDF,
                                            " en la ruta ",
                                            gestionPDF.directorio_destino.Path);


                MessageDialog popup = new MessageDialog(resultado);
                popup.ShowAsync();



            }
            catch (Exception ex)
            {
                resultado = ex.Message;
            }

            return resultado;
        }
    }
}
